package es.d2d.beanwrapper.dto;


public class BaseTestDTO {
    private Long id;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

}
