package es.d2d.beanwrapper.dto;

public interface DatabaseClass<T> {
    public T getDatabaseValue();
}
