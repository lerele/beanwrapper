package es.d2d.beanwrapper.propertyeditor;


public interface IPropertyEditor {
    public Object getValue(Object value);
}
